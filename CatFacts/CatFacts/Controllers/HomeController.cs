﻿using CatFacts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;


namespace CatFacts.Controllers
{
    public class HomeController : Controller
    {
        static HttpClient client = new HttpClient();
        private static readonly JsonSerializer JsonSerializer = new JsonSerializer();

        public ActionResult Index()
        {
            return View("Index", new List<JsonFact>());
        }

        [HttpGet]
        public async Task<ActionResult> GetData(string path)
        {
            try
            {
                var result = await GetDataAsync(path);
                var context = new CatsDBEntities();
                foreach (var item in result)
                {
                    var fact = new Facts(item);
                    context.Facts.Add(fact);
                    await context.SaveChangesAsync();
                }
                
                return View("Index", result);
            }
            catch (Exception e)
            {                
                return View("Index", new List<JsonFact>());
            }
        }

        public async Task<List<JsonFact>> GetDataAsync(string path)
        {
            using (var httpClient = new HttpClient())
            {             
                var response = await httpClient.GetAsync(path);
                var responseStr = await response.Content.ReadAsStringAsync();
                var responseObj = JsonConvert.DeserializeObject<IEnumerable<JsonFact>>(responseStr)
                    .OrderBy(a => a.updatedAt).ToList();
                return responseObj;
            }           
        }

     
    }
}