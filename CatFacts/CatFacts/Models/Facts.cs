//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CatFacts.Models
{
    using System;
    using System.Collections.Generic;

    public partial class Facts
    {
        public string Id { get; set; }
        public int FactVersion { get; set; }
        public string UserId { get; set; }
        public string FactText { get; set; }
        public System.DateTime UpdatedAt { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public bool Deleted { get; set; }
        public string FactSource { get; set; }
        public string AnimalType { get; set; }
        public bool StatusVerified { get; set; }
        public int StatusSentCount { get; set; }

        public Facts(JsonFact item)
        {
            Id = item._id;
            FactVersion = item.__v;
            UserId = item.user;
            UpdatedAt = item.updatedAt;
            CreatedAt = item.createdAt;
            Deleted = item.deleted;
            FactSource = item.source;
            AnimalType = item.type;
            FactText = item.text;
            StatusVerified = item.status.verified;
            StatusSentCount = item.status.sentCount;
        }
    }
}