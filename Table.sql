USE [CatsDB]
GO

/****** Object:  Table [dbo].[Facts]     ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Facts](
	[Id] [varchar](24) NOT NULL,
	[FactVersion] [int] NOT NULL,
	[UserId] [varchar](24) NOT NULL,
	[FactText] [nvarchar](max) NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[FactSource] [nvarchar](100) NULL,
	[AnimalType] [nvarchar](20) NULL,
	[StatusVerified] [bit] NOT NULL,
	[StatusSentCount] [int] NOT NULL,
 CONSTRAINT [PK__Facts__3214EC0759F3612B] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Facts] ADD  CONSTRAINT [DF_Facts_FactVersion]  DEFAULT ((0)) FOR [FactVersion]
GO

ALTER TABLE [dbo].[Facts] ADD  CONSTRAINT [DF_Facts_UpdatedAt]  DEFAULT (getdate()) FOR [UpdatedAt]
GO

ALTER TABLE [dbo].[Facts] ADD  CONSTRAINT [DF_Facts_SendDate]  DEFAULT (getdate()) FOR [CreatedAt]
GO

ALTER TABLE [dbo].[Facts] ADD  CONSTRAINT [DF_Facts_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO

ALTER TABLE [dbo].[Facts] ADD  CONSTRAINT [DF_Facts_StatusVerified]  DEFAULT ((0)) FOR [StatusVerified]
GO

ALTER TABLE [dbo].[Facts] ADD  CONSTRAINT [DF_Facts_StatusSentCount]  DEFAULT ((0)) FOR [StatusSentCount]
GO



/****** Object:  Index [UserId_IX]  ******/
CREATE NONCLUSTERED INDEX [UserId_IX] ON [dbo].[Facts]
(
	[UserId] ASC
)
INCLUDE([Id],[FactVersion],[FactText],[UpdatedAt],[CreatedAt],[Deleted],[FactSource],[AnimalType],[StatusVerified],[StatusSentCount]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [CreatedAt_IX]  ******/
CREATE NONCLUSTERED INDEX [CreatedAt_IX] ON [dbo].[Facts]
(
	[CreatedAt] ASC
)
INCLUDE([Id],[FactVersion],[UserId],[FactText],[UpdatedAt],[Deleted],[FactSource],[AnimalType],[StatusVerified],[StatusSentCount]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO